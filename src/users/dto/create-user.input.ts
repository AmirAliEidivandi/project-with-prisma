import { Field, InputType } from '@nestjs/graphql';
import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';

@InputType()
export class SignupDto {
  @Field(() => String)
  @IsString({ message: 'Fullname must be a string' })
  @IsNotEmpty({ message: 'Fullname is required' })
  fullname: string;

  @Field(() => String)
  @IsString({ message: 'email must be a string' })
  @IsEmail()
  @IsNotEmpty({ message: 'email is required' })
  email: string;

  @Field(() => String)
  @IsString()
  @MinLength(8, { message: 'Password must be at least 8 characters' })
  @IsNotEmpty({ message: 'Fullname is required' })
  password: string;
}
