import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { SignupDto } from './dto/create-user.input';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async register(signupDto: SignupDto) {
    const hashedPassword = await bcrypt.hash(signupDto.password, 10);
    const user = await this.prisma.user.create({
      data: {
        fullname: signupDto.fullname,
        password: hashedPassword,
        email: signupDto.email,
      },
    });
    return user;
  }
}
