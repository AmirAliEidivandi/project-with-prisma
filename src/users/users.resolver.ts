import { Resolver, Mutation, Args, Query } from '@nestjs/graphql';
import { UsersService } from './users.service';
import { SignupDto } from './dto/create-user.input';

@Resolver('User')
export class UsersResolver {
  constructor(private readonly usersService: UsersService) {}

  @Mutation()
  async signup(@Args('signupInput') signupDto: SignupDto) {
    return await this.usersService.register(signupDto);
  }

  @Query()
  hello() {
    return 'hello world :)';
  }
}
